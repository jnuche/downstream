import logging
import sys


# Adapted from https://stackoverflow.com/a/56944256
class DefaultFormatter(logging.Formatter):
    grey = '\x1b[38;20m'
    yellow = '\x1b[33;20m'
    red = '\x1b[31;20m'
    reset = '\x1b[0m'
    format = '%(asctime)s %(levelname)s %(message)s'

    FORMATS = {
        logging.DEBUG: grey + format + reset,
        logging.INFO: grey + format + reset,
        logging.WARNING: yellow + format + reset,
        logging.ERROR: red + format + reset,
        logging.CRITICAL: red + format + reset
    }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(fmt=log_fmt, datefmt='%H:%M:%S')
        return formatter.format(record)


def set_up_logging():
    logging.basicConfig(level=logging.INFO, stream=sys.stdout)
    # logging.getLogger("urllib3").setLevel(logging.WARNING)
    logging.root.handlers[0].setFormatter(DefaultFormatter())
