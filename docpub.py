#!/usr/bin/python3

import logging
import os
import subprocess
import sys
import time
import zipfile

import jwt
import requests
from jwt import PyJWKClient, InvalidTokenError

from lib.log import set_up_logging


def api_get(path):
    res = requests.get(
        f"https://{gitlab_host}/api/v4/{path}",
        headers={'JOB-TOKEN': os.getenv('CI_JOB_TOKEN')},
        allow_redirects=True
    )
    res.raise_for_status()
    return res


def decode_jwt():
    jwks_client = PyJWKClient(f"https://{gitlab_host}/-/jwks")

    try:
        token = os.getenv('DOC_PRJ_JWT')
        return jwt.decode(
            token,
            jwks_client.get_signing_key_from_jwt(token).key,
            jwt.get_unverified_header(token)['alg'],
            audience='doc-publisher'
        )
    except InvalidTokenError as e:
        logging.error("Token invalid: %s", e)
        sys.exit(1)


def validate_publish_location():
    project_path = token_data['project_path']
    if not pub_location.startswith(project_path):
        logging.error("""Publishing location "%s" is invalid for project "%s" """, pub_location, project_path)
        sys.exit(1)


def wait_for_upstream_pipeline():
    # Wait for up to 10m
    deadline = time.time() + 600

    logging.info('Waiting for upstream pipeline to complete')

    pipeline_status = None
    while time.time() < deadline:
        try:
            pipeline_res = api_get(f"projects/{project_id}/pipelines/{pipeline_id}")

            pipeline_status = pipeline_res.json()['status']
            if pipeline_status in ['success', 'failed', 'canceled', 'skipped']:
                break
            time.sleep(10)
        except Exception as e:
            logging.error("Failed to determine status of upstream pipeline: %s", e)
            sys.exit(1)

    if pipeline_status != 'success':
        logging.warning('Upstream pipeline did not complete successfully. Finishing now without publishing')
        sys.exit()
    else:
        logging.info('Upstream pipeline completed')


def get_docs():
    logging.info("Retrieving documentation from upstream pipeline")

    try:
        job_res = api_get(f"projects/{project_id}/jobs/{job_id}/artifacts")

        artifacts_zip = 'artifacts.zip'
        with open(artifacts_zip, 'wb') as artsf:
            artsf.write(job_res.content)
        with zipfile.ZipFile(artifacts_zip, 'r') as artsf:
            artsf.extractall(artifacts_dir)
    except Exception as e:
        logging.error("Failed to retrieve documentation from upstream pipeline: %s", e)
        sys.exit(1)

    if os.path.isdir(docs_dir) and len(os.listdir(docs_dir)) > 0:
        logging.info('Documentation retrieved')
    else:
        logging.error("""Specified docs directory "%s" not found among the upstream artifacts or is empty""")
        sys.exit(1)


def publish_docs():
    def create_docs_dir_at_destination():
        """
        Rsync empty dir to ensure the publishing location exists on the target
        """
        try:
            pwd = os.getcwd()
            os.makedirs(f"/tmp/{pub_location}")
            os.chdir('/tmp')
            # TODO: must publish to doc.discovery.wmnet
            print("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
            print('rsync', '--recursive', '--relative', pub_location, 'rsync://doc1003.eqiad.wmnet/gitlab-doc/')
            print("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
            # subprocess.check_call([
            #     'rsync', '--recursive', '--relative', pub_location, 'rsync://doc1003.eqiad.wmnet/gitlab-doc/'
            # ])
            os.chdir(pwd)
        except Exception as e:
            logging.error("Failed to create directory for documentation at target destination: %s", e)
            sys.exit(1)

    def rsync_docs():
        try:
            # Note that `--archive` will preserve permissions and dates, but owner and group are assigned by the
            # rsync module on the target (i.e. `doc-uploader`)
            # TODO: must publish to doc.discovery.wmnet
            # subprocess.check_call([
            #     'rsync', '--archive', '--compress', '--delete-after',
            #     docs_dir + '/', f"rsync://doc1003.eqiad.wmnet/gitlab-doc/{pub_location}/"
            # ])
            print("BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB")
            print('rsync', '--archive', '--compress', '--delete-after',
                  docs_dir + '/', f"rsync://doc1003.eqiad.wmnet/gitlab-doc/{pub_location}/")
            print("BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB")
        except subprocess.CalledProcessError as e:
            logging.error("Failed to publish documentation: %s", e)
            sys.exit(1)

    logging.info("Publishing documentation")

    create_docs_dir_at_destination()
    rsync_docs()


artifacts_dir = 'arts'
docs_dir = os.path.join(artifacts_dir, os.getenv('DOC_PRJ_DOCS_DIR'))
gitlab_host = os.getenv('CI_SERVER_HOST')
pub_location = os.getenv('PUB_LOCATION')

set_up_logging()
token_data = decode_jwt()
validate_publish_location()

project_id, pipeline_id, job_id = [token_data[key] for key in ['project_id', 'pipeline_id', 'job_id']]
wait_for_upstream_pipeline()
get_docs()
publish_docs()
